<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Laravel</title>

  <!-- Fonts -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" >
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" >
  <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
  <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
  <!-- Styles -->
  <!-- Styles -->
  <style>
  html, body {
    background-color: #ddd;
    color: #636b6f;
    font-family: 'Raleway', sans-serif;
    font-weight: 100;
    height: 100vh;
    margin: 0;
  }

  .full-height {
    height: 100vh;
  }

  .flex-center {
    align-items: center;
    display: flex;
    justify-content: center;
  }

  .position-ref {
    position: relative;
  }

  .top-right {
    position: absolute;
    right: 10px;
    top: 18px;
  }

  .content {
    text-align: center;
  }

  .title {
    font-size: 84px;
  }

  .links > a {
    color: #636b6f;
    padding: 0 25px;
    font-size: 12px;
    font-weight: 600;
    letter-spacing: .1rem;
    text-decoration: none;
    text-transform: uppercase;
  }

  .m-b-md {
    margin-bottom: 30px;
  }
  </style>
</head>
<body>
  <div class="flex-center position-ref full-height">


    <div class="content" id="app">

      <div class="row">
        <div class="col-sm-6">

          <h1 style="margin-top:18px;">@{{cardsfirst}}</h1>
        </div>
        <div class="col-sm-6">
          <div class="moveup pull-left">
          <h2> <a @click="moveup()"><i class="fa fa-angle-up"></i></a> </h2>
          </div>
          <div class="movedown pull-left">
           <h2> <a @click="movedown()"> <i class="fa fa-angle-down"></i></a> </h2>
          </div>
        </div>
      </div>

    </div>
    <script type="text/javascript">
    var app = new Vue({
      el:'#app',
      data:{
        cards:[],
        cardsfirst:'',
        cardsid:'',

      },
      methods:{

        isExist : function(){
          for(var i=0; i < this.cards.length; i++){

            if( this.cards[i] == this.cardsid){

              return true
            }

          }
          return false
        },

        moveup:function(){

         if (this.cardsfirst=='A' ) {
           this.cardsid = this.cardsid+1;
           this.cardsfirst=this.cards[this.cardsid];
         }
         else if (this.cardsfirst=='K') {
           this.cardsid = 0;
           this.cardsfirst=this.cards[this.cardsid];
         }
         else{
           this.cardsid = this.cardsid+1;
           this.cardsfirst=this.cards[this.cardsid];
         }
        },
        movedown: function(){

          if (this.cardsfirst=='A' ) {
            this.cardsid= this.cards.length;
            this.cardsfirst=this.cards[this.cards.length];
          }
          else if (this.cardsfirst=='K') {
            this.cardsid= this.cardsid -1;
            this.cardsfirst=this.cards[this.cardsid];
          }
          else{
            this.cardsid= this.cardsid -1;
            this.cardsfirst=this.cards[this.cardsid];
          }
        },

      },
      mounted(){

        axios.get('https://cards.davidneal.io/api/cards').then(response=>{
          //console.log(response.data)
          this.cards=response.data;
           for(i=0; i<response.data.length;i++ ) {
             //console.log(response.data[i].value)
             this.cards[i]=response.data[i].value;
           }
           this.cardsfirst=this.cards[0];
           this.cardsid=0;
        }).catch(error=>{
          console.log(error)
          swal('oops...','Something went wrong','error')
        })
      },



    })
    </script>
  </body>
  </html>
